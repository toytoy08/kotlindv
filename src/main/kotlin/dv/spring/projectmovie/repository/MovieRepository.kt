package dv.spring.projectmovie.repository

import dv.spring.projectmovie.entity.Movie
import org.springframework.data.repository.CrudRepository

interface MovieRepository : CrudRepository<Movie, Long>
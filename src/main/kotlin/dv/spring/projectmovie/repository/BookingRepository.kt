package dv.spring.projectmovie.repository

import dv.spring.projectmovie.entity.Booking
import dv.spring.projectmovie.entity.Showtime
import org.springframework.data.repository.CrudRepository

interface BookingRepository : CrudRepository<Booking, Long>{
    fun findById (id:Long?) : List<Booking>
}
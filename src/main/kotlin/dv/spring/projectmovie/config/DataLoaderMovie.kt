package dv.spring.projectmovie.config

import dv.spring.projectmovie.entity.*
import dv.spring.projectmovie.repository.*
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.ApplicationArguments
import org.springframework.boot.ApplicationRunner
import org.springframework.stereotype.Component
import javax.transaction.Transactional

@Component
class DataLoaderMovie : ApplicationRunner {
    @Autowired
    lateinit var movieRepository: MovieRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var seatRepository: SeatRepository
    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository
    @Autowired
    lateinit var bookingRepository: BookingRepository
    @Autowired
    lateinit var historyRepository: HistoryRepository

    @Transactional
    override fun run(args: ApplicationArguments?) {
        var movietime1 = 7500000
        var movietime2 = 6300000
        var movietime3 = 7800000
        var movietime4 = 7200000
        var movietime5 = 6000000
        var movietime6 = 4800000
        var movietime7 = 7800000

        var day1 = 86400000
        var day2 = 86400000 * 2
        var day3 = 86400000 * 3

        var movie1 = movieRepository.save(Movie(
                "อลิตา แบทเทิล แองเจิ้ล",
                125,
                "https://lh3.googleusercontent.com/m-KIGZSYMapNnCIGB3QVXPlqkv7Vu_-VoEXqdleoNZ-CDRVdprMcoUanKbMHeFjlXk38FYPxW0Gar0XEQYI=w1024",
                Soundtrack.EN_TH,
                Subtitle.TH
        ))
        var movie2 = movieRepository.save(Movie(
                "อภินิหารไวกิ้งพิชิตมังกร 3",
                105,
                "https://lh3.googleusercontent.com/xTpxPyQOfsnpA2lkp8Fz7LjVA-hsadGF0U7c5rwJq4ikRG_np-5_kO-gzHMVkur2Y5BnLQ4Sfki_Qj7T0Mlisw=w1024",
                Soundtrack.EN_TH,
                Subtitle.TH
        ))

        var movie3 = movieRepository.save(Movie(
                "Captain Marvel",
                130,
                "https://lh3.googleusercontent.com/3Ost-FvXYMVAs84hLxJAn9qqIh9s_YCC8oCySCuOWIJWIu8zPIuKYJ9FG_FEFBqKZoQnguVOuNAkg9kArkPM=w1024",
                Soundtrack.EN_TH,
                Subtitle.TH
        ))

        var movie4 = movieRepository.save(Movie(
                "เฟรนด์โซน ระวัง..สิ้นสุดทางเพื่อน",
                120,
                "https://lh3.googleusercontent.com/hGX4-13iqHM1Kx_nyj67AtCsFXAjJyS0wxoEcJynOs99BUJXQWLIUQFcsje3jdMW75wtrJU2ktywEF3gxoKe=w1024",
                Soundtrack.TH,
                Subtitle.EN
        ))

        var movie5 = movieRepository.save(Movie(
                "สุขสันต์วันตาย 2U",
                100,
                "https://lh3.googleusercontent.com/8hKivfDWOC2G8rQolX9850yiPWs5dJzSgyGirJFjU66jeUPV9-5gkm1soLxlHubI5_V_lK1T0vdfinsUlVZtOA=w1024",
                Soundtrack.EN_TH,
                Subtitle.TH
        ))

        var movie6 = movieRepository.save(Movie(
                "คุซามะ อินฟินิตี้",
                80,
                "https://lh3.googleusercontent.com/Fs3-9D0HjOaP-BWhOhVp-gZAqN3aRyyB5Z7W59f67v_frABpaljFUhFR9Pjs-fTN37jz1mqml9LRUK1tVjjFBg=w1024",
                Soundtrack.EN,
                Subtitle.TH
        ))

        var movie7 = movieRepository.save(Movie(
                "คนเหนือมนุษย์",
                130,
                "https://lh3.googleusercontent.com/rVCTrkvoC4Iwpv4sPY8gSGYom0rKm28exIaVBD2sGYUip0SMuKqfDxyAVrd_Ps_H39Ss-NGT1nzytQPuBunr=w1024",
                Soundtrack.EN_TH,
                Subtitle.TH
        ))

        var user1 = userRepository.save(User(
                "test1@gmail.com",
                "kkjjjjjak",
                "aaa",
                "https://i.imgur.com/xoFYkyq.png"
        ))

        var user2 = userRepository.save(User(
                "test2@gmail.com",
                "asd",
                "qwe",
                "https://i.imgur.com/xoFYkyq.png"
        ))

        var user3 = userRepository.save(User(
                "Red@gmail.com",
                "Red",
                "Color",
                "https://i.imgur.com/xoFYkyq.png"
        ))

        var user4 = userRepository.save(User(
                "Mario@gmail.com",
                "Mario",
                "Bros",
                "https://i.imgur.com/xoFYkyq.png"
        ))

        var user5 = userRepository.save(User(
                "Tertis@gmail.com",
                "Ter",
                "Tis",
                "https://i.imgur.com/xoFYkyq.png"
        ))

        var user6 = userRepository.save(User(
                "QWERTY@gmail.com",
                "QWE",
                "RTY",
                "https://i.imgur.com/xoFYkyq.png"
        ))

        var user7 = userRepository.save(User(
                "copyuser@gmail.com",
                "user",
                "fake",
                "https://i.imgur.com/xoFYkyq.png"
        ))

        var user8 = userRepository.save(User(
                "MovieTic@gmail.com",
                "Movie",
                "Tic",
                "https://i.imgur.com/xoFYkyq.png"
        ))

        var seat1 = seatRepository.save(Seats(
                "Premium",
                SeatType.PREMIUM,
                190,
                4,
                20
        ))

        var seat2 = seatRepository.save(Seats(
                "Deluxe",
                SeatType.DELUXE,
                150,
                10,
                20
        ))

        var seat3 = seatRepository.save(Seats(
                "Premium",
                SeatType.PREMIUM,
                190,
                5,
                10
        ))

        var seat4 = seatRepository.save(Seats(
                "Deluxe",
                SeatType.DELUXE,
                150,
                10,
                10
        ))

        var seat5 = seatRepository.save(Seats(
                "Premium",
                SeatType.PREMIUM,
                190,
                3,
                17
        ))

        var seat6 = seatRepository.save(Seats(
                "Deluxe",
                SeatType.DELUXE,
                150,
                2,
                12
        ))

        var seat7 = seatRepository.save(Seats(
                "Premium",
                SeatType.PREMIUM,
                190,
                6,
                16
        ))

        var seat8 = seatRepository.save(Seats(
                "Deluxe",
                SeatType.DELUXE,
                150,
                1,
                17
        ))

        var showtime1_1 = showtimeRepository.save(Showtime(1553565600000, 1553565600000 + movietime1, Soundtrack.TH, Subtitle.TH))
        var showtime1_2 = showtimeRepository.save(Showtime(1553568000000 + day1, 1553568000000 + movietime1, Soundtrack.EN, Subtitle.TH))
        var showtime1_3 = showtimeRepository.save(Showtime(1553574000000 + day1, 1553574000000 + movietime1, Soundtrack.TH, Subtitle.TH))
        var showtime1_4 = showtimeRepository.save(Showtime(1553576400000 + day2, 1553576400000 + movietime1, Soundtrack.EN, Subtitle.TH))
        var showtime1_5 = showtimeRepository.save(Showtime(1553578200000 + day2, 1553578200000 + movietime1, Soundtrack.TH, Subtitle.TH))
        var showtime1_6 = showtimeRepository.save(Showtime(1553580000000 + day3, 1553580000000 + movietime1, Soundtrack.EN, Subtitle.TH))
        var showtime1_7 = showtimeRepository.save(Showtime(1553590800000 + day3, 1553590800000 + movietime1, Soundtrack.TH, Subtitle.TH))

        var showtime2_1 = showtimeRepository.save(Showtime(1553565600000, 1553565600000 + movietime2, Soundtrack.TH, Subtitle.TH))
        var showtime2_2 = showtimeRepository.save(Showtime(1553568000000 + day1, 1553568000000 + movietime2, Soundtrack.EN, Subtitle.TH))
        var showtime2_3 = showtimeRepository.save(Showtime(1553574000000 + day1, 1553574000000 + movietime2, Soundtrack.TH, Subtitle.TH))
        var showtime2_4 = showtimeRepository.save(Showtime(1553576400000 + day2, 1553576400000 + movietime2, Soundtrack.EN, Subtitle.TH))
        var showtime2_5 = showtimeRepository.save(Showtime(1553578200000 + day2, 1553578200000 + movietime2, Soundtrack.TH, Subtitle.TH))
        var showtime2_6 = showtimeRepository.save(Showtime(1553580000000 + day3, 1553580000000 + movietime2, Soundtrack.EN, Subtitle.TH))
        var showtime2_7 = showtimeRepository.save(Showtime(1553590800000 + day3, 1553590800000 + movietime2, Soundtrack.TH, Subtitle.TH))

        var showtime3_1 = showtimeRepository.save(Showtime(1553565600000, 1553565600000 + movietime3, Soundtrack.TH, Subtitle.TH))
        var showtime3_2 = showtimeRepository.save(Showtime(1553568000000 + day1, 1553568000000 + movietime3, Soundtrack.EN, Subtitle.TH))
        var showtime3_3 = showtimeRepository.save(Showtime(1553574000000 + day1, 1553574000000 + movietime3, Soundtrack.TH, Subtitle.TH))
        var showtime3_4 = showtimeRepository.save(Showtime(1553576400000 + day2, 1553576400000 + movietime3, Soundtrack.EN, Subtitle.TH))
        var showtime3_5 = showtimeRepository.save(Showtime(1553578200000 + day2, 1553578200000 + movietime3, Soundtrack.TH, Subtitle.TH))
        var showtime3_6 = showtimeRepository.save(Showtime(1553580000000 + day3, 1553580000000 + movietime3, Soundtrack.EN, Subtitle.TH))
        var showtime3_7 = showtimeRepository.save(Showtime(1553590800000 + day3, 1553590800000 + movietime3, Soundtrack.TH, Subtitle.TH))

        var showtime4_1 = showtimeRepository.save(Showtime(1553565600000, 1553565600000 + movietime4, Soundtrack.TH, Subtitle.TH))
        var showtime4_2 = showtimeRepository.save(Showtime(1553568000000 + day1, 1553568000000 + movietime4, Soundtrack.EN, Subtitle.TH))
        var showtime4_3 = showtimeRepository.save(Showtime(1553574000000 + day1, 1553574000000 + movietime4, Soundtrack.TH, Subtitle.TH))
        var showtime4_4 = showtimeRepository.save(Showtime(1553576400000 + day2, 1553576400000 + movietime4, Soundtrack.EN, Subtitle.TH))
        var showtime4_5 = showtimeRepository.save(Showtime(1553578200000 + day2, 1553578200000 + movietime4, Soundtrack.TH, Subtitle.TH))
        var showtime4_6 = showtimeRepository.save(Showtime(1553580000000 + day3, 1553580000000 + movietime4, Soundtrack.EN, Subtitle.TH))
        var showtime4_7 = showtimeRepository.save(Showtime(1553590800000 + day3, 1553590800000 + movietime4, Soundtrack.TH, Subtitle.TH))

        var showtime5_1 = showtimeRepository.save(Showtime(1553565600000, 1553565600000 + movietime5, Soundtrack.TH, Subtitle.TH))
        var showtime5_2 = showtimeRepository.save(Showtime(1553568000000 + day1, 1553568000000 + movietime5, Soundtrack.EN, Subtitle.TH))
        var showtime5_3 = showtimeRepository.save(Showtime(1553574000000 + day1, 1553574000000 + movietime5, Soundtrack.TH, Subtitle.TH))
        var showtime5_4 = showtimeRepository.save(Showtime(1553576400000 + day2, 1553576400000 + movietime5, Soundtrack.EN, Subtitle.TH))
        var showtime5_5 = showtimeRepository.save(Showtime(1553578200000 + day2, 1553578200000 + movietime5, Soundtrack.TH, Subtitle.TH))
        var showtime5_6 = showtimeRepository.save(Showtime(1553580000000 + day3, 1553580000000 + movietime5, Soundtrack.EN, Subtitle.TH))
        var showtime5_7 = showtimeRepository.save(Showtime(1553590800000 + day3, 1553590800000 + movietime5, Soundtrack.TH, Subtitle.TH))

        var showtime6_1 = showtimeRepository.save(Showtime(1553565600000, 1553565600000 + movietime6, Soundtrack.TH, Subtitle.TH))
        var showtime6_2 = showtimeRepository.save(Showtime(1553568000000 + day1, 1553568000000 + movietime6, Soundtrack.EN, Subtitle.TH))
        var showtime6_3 = showtimeRepository.save(Showtime(1553574000000 + day1, 1553574000000 + movietime6, Soundtrack.TH, Subtitle.TH))
        var showtime6_4 = showtimeRepository.save(Showtime(1553576400000 + day2, 1553576400000 + movietime6, Soundtrack.EN, Subtitle.TH))
        var showtime6_5 = showtimeRepository.save(Showtime(1553578200000 + day2, 1553578200000 + movietime6, Soundtrack.TH, Subtitle.TH))
        var showtime6_6 = showtimeRepository.save(Showtime(1553580000000 + day3, 1553580000000 + movietime6, Soundtrack.EN, Subtitle.TH))
        var showtime6_7 = showtimeRepository.save(Showtime(1553590800000 + day3, 1553590800000 + movietime6, Soundtrack.TH, Subtitle.TH))

        var showtime7_1 = showtimeRepository.save(Showtime(1553565600000, 1553565600000 + movietime7, Soundtrack.TH, Subtitle.TH))
        var showtime7_2 = showtimeRepository.save(Showtime(1553568000000 + day1, 1553568000000 + movietime7, Soundtrack.EN, Subtitle.TH))
        var showtime7_3 = showtimeRepository.save(Showtime(1553574000000 + day1, 1553574000000 + movietime7, Soundtrack.TH, Subtitle.TH))
        var showtime7_4 = showtimeRepository.save(Showtime(1553576400000 + day2, 1553576400000 + movietime7, Soundtrack.EN, Subtitle.TH))
        var showtime7_5 = showtimeRepository.save(Showtime(1553578200000 + day2, 1553578200000 + movietime7, Soundtrack.TH, Subtitle.TH))
        var showtime7_6 = showtimeRepository.save(Showtime(1553580000000 + day3, 1553580000000 + movietime7, Soundtrack.EN, Subtitle.TH))
        var showtime7_7 = showtimeRepository.save(Showtime(1553590800000 + day3, 1553590800000 + movietime7, Soundtrack.TH, Subtitle.TH))

        var booking1 = bookingRepository.save(Booking(1551190328167))
        var booking2 = bookingRepository.save(Booking(1551194901646))
        var booking3 = bookingRepository.save(Booking(1551194909494))
        var booking4 = bookingRepository.save(Booking(1551205027089))
        var booking5 = bookingRepository.save(Booking(1553516162331))

        showtime1_1.movie = movie1
        showtime1_2.movie = movie1
        showtime1_3.movie = movie1
        showtime1_4.movie = movie1
        showtime1_5.movie = movie1
        showtime1_6.movie = movie1
        showtime1_7.movie = movie1
        showtime1_1.seats.add(seat1)
        showtime1_1.seats.add(seat2)

        showtime2_1.movie = movie2
        showtime2_2.movie = movie2
        showtime2_3.movie = movie2
        showtime2_4.movie = movie2
        showtime2_5.movie = movie2
        showtime2_6.movie = movie2
        showtime2_7.movie = movie2
        showtime2_3.seats.add(seat4)

        showtime3_1.movie = movie3
        showtime3_2.movie = movie3
        showtime3_3.movie = movie3
        showtime3_4.movie = movie3
        showtime3_5.movie = movie3
        showtime3_6.movie = movie3
        showtime3_7.movie = movie3
        showtime3_6.seats.add(seat5)
        showtime3_6.seats.add(seat6)

        showtime4_1.movie = movie4
        showtime4_2.movie = movie4
        showtime4_3.movie = movie4
        showtime4_4.movie = movie4
        showtime4_5.movie = movie4
        showtime4_6.movie = movie4
        showtime4_7.movie = movie4
        showtime4_4.seats.add(seat7)

        showtime5_1.movie = movie5
        showtime5_2.movie = movie5
        showtime5_3.movie = movie5
        showtime5_4.movie = movie5
        showtime5_5.movie = movie5
        showtime5_6.movie = movie5
        showtime5_7.movie = movie5
        showtime5_7.seats.add(seat8)

        showtime6_1.movie = movie6
        showtime6_2.movie = movie6
        showtime6_3.movie = movie6
        showtime6_4.movie = movie6
        showtime6_5.movie = movie6
        showtime6_6.movie = movie6
        showtime6_7.movie = movie6
        showtime6_2.seats.add(seat3)

        showtime7_1.movie = movie7
        showtime7_2.movie = movie7
        showtime7_3.movie = movie7
        showtime7_4.movie = movie7
        showtime7_5.movie = movie7
        showtime7_6.movie = movie7
        showtime7_7.movie = movie7

        seat1.user = user1
        seat2.user = user2
        seat3.user = user3
        seat4.user = user4
        seat5.user = user5
        seat6.user = user6
        seat7.user = user7
        seat8.user = user8

        booking1.user.add(user1)
        booking1.showtime = showtime1_1
        booking1.seats.add(seat1)

        booking2.user.add(user2)
        booking2.showtime = showtime2_3
        booking2.seats.add(seat2)

        booking3.user.add(user3)
        booking3.showtime = showtime3_6
        booking3.seats.add(seat3)

        booking4.user.add(user4)
        booking4.showtime = showtime5_7
        booking4.seats.add(seat4)

        booking5.user.add(user5)
        booking5.showtime = showtime6_2
        booking5.seats.add(seat5)


        var history1 = historyRepository.save(History(
                booking1
        ))

        var history2 = historyRepository.save(History(
                booking2
        ))

        var history3 = historyRepository.save(History(
                booking3
        ))

        var history4 = historyRepository.save(History(
                booking4
        ))

        var history5 = historyRepository.save(History(
                booking5
        ))
    }
}
package dv.spring.projectmovie.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class User(var email: String? = null,
                var firstName: String? = null,
                var lastName: String? = null,
                var image: String? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null
}
package dv.spring.projectmovie.service

import dv.spring.projectmovie.dao.UserDao
import dv.spring.projectmovie.entity.User
import dv.spring.projectmovie.entity.dto.UserDto
import dv.spring.projectmovie.util.MapperMovie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class UserServiceImpl:UserService{
    override fun save(user: UserDto): User {
        val userSer = MapperMovie.MOVIEINSTANCE.mapUserDto(user)
        return userDao.save(userSer)
    }

    override fun getUsers(): List<User> {
        return userDao.getUsers()
    }

    @Autowired
    lateinit var userDao : UserDao
}
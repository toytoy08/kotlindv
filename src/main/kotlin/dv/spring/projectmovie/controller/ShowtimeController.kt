package dv.spring.projectmovie.controller

import dv.spring.projectmovie.service.ShowtimeService
import dv.spring.projectmovie.util.MapperMovie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RestController

@RestController

class ShowtimeController {
    @Autowired
    lateinit var showtimeService: ShowtimeService

    @GetMapping("/showtime/list")
    fun getShowtimes(): ResponseEntity<Any> {
        val showtimes = showtimeService.getShowtimes()
        return ResponseEntity.ok(MapperMovie.MOVIEINSTANCE.mapShowtimeList(showtimes))
    }

    @GetMapping("showtime/movie/{movieId}")
    fun getShowtimeById(@PathVariable("movieId") id: Long?): ResponseEntity<Any> {
        val showtime = showtimeService.getShowtimeById(id)
        return ResponseEntity.ok(MapperMovie.MOVIEINSTANCE.mapShowtimeList(showtime))
    }

//    @GetMapping("showtime/{showId}")
//    fun getOneShowtimeById(@PathVariable("showId") id: Long?): ResponseEntity<Any> {
//        val showtime = showtimeService.getOneShowtimeById(id)
//        return ResponseEntity.ok(MapperMovie.MOVIEINSTANCE.mapShowtimeList(showtime))
//    }
}
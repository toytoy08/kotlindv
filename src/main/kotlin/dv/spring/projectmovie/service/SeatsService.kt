package dv.spring.projectmovie.service

import dv.spring.projectmovie.entity.Seats

interface SeatsService{
    fun getSeats(): List<Seats>

}
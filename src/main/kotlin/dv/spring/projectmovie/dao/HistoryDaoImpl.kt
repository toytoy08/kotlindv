package dv.spring.projectmovie.dao

import dv.spring.projectmovie.entity.History
import dv.spring.projectmovie.repository.HistoryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class HistoryDaoImpl : HistoryDao {
    override fun getHistory(): List<History> {
        return historyRepository.findAll().filterIsInstance(History::class.java)
    }

    @Autowired
    lateinit var historyRepository: HistoryRepository
}
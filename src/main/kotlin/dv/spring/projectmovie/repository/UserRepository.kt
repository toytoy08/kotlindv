package dv.spring.projectmovie.repository

import dv.spring.projectmovie.entity.User
import org.springframework.data.repository.CrudRepository

interface UserRepository : CrudRepository<User, Long>{
    fun findById (id:Long?):User
}
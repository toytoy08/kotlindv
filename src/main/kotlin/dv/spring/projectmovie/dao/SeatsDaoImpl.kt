package dv.spring.projectmovie.dao

import dv.spring.projectmovie.entity.Seats
import dv.spring.projectmovie.repository.SeatRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile ("db")
@Repository
class SeatsDaoImpl : SeatsDao{
    override fun getSeats(): List<Seats> {
        return seatRepository.findAll().filterIsInstance(Seats::class.java)
    }

    @Autowired
    lateinit var seatRepository: SeatRepository
}
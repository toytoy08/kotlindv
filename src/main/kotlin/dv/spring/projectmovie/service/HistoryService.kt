package dv.spring.projectmovie.service

import dv.spring.projectmovie.entity.History

interface HistoryService{
    fun getHistory(): List<History>

}
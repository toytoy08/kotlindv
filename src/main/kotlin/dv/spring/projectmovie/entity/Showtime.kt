package dv.spring.projectmovie.entity

import javax.persistence.*

@Entity
data class Showtime(var startDateTime: Long? = null,
                    var endDateTime: Long? = null,
                    var soundtrack: Soundtrack? = null,
                    var subtitle: Subtitle? = null
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @ManyToOne
    var movie: Movie? = null

    @OneToMany
    var seats = mutableListOf<Seats>()
}

package dv.spring.projectmovie.entity.dto

import dv.spring.projectmovie.entity.Seats
import dv.spring.projectmovie.entity.User

data class BookingDto(
//        var user: List<User>? = null,
        var showtime: ShowtimeDto? = null,
        var createdDateTime: Long? = null,
        var seats: List<SeatsDto>? = null,
        var id: Long? = null
)
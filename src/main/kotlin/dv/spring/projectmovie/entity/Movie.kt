package dv.spring.projectmovie.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class Movie(var name: String? = null,
                 var duration: Int? = null,
                 var image: String? = null,
                 var soundtrack: Soundtrack? = null,
                 var subtitle: Subtitle? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null

}
package dv.spring.projectmovie.entity

enum class SeatType {
    DELUXE, PREMIUM, SOFA
}
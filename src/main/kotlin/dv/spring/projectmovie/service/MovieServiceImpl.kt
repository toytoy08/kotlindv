package dv.spring.projectmovie.service

import dv.spring.projectmovie.dao.MovieDao
import dv.spring.projectmovie.entity.Movie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class MovieServiceImpl: MovieService {
    override fun getMovies(): List<Movie> {
        return movieDao.getMovies()
    }

    @Autowired
    lateinit var movieDao: MovieDao
}
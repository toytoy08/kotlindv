package dv.spring.projectmovie.controller

import dv.spring.projectmovie.service.MovieService
import dv.spring.projectmovie.util.MapperMovie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController

class MovieController {
    @Autowired
    lateinit var movieService: MovieService

    @GetMapping("/movie/list")
    fun getMovies(): ResponseEntity<Any> {
        val movies = movieService.getMovies()
        return ResponseEntity.ok(MapperMovie.MOVIEINSTANCE.mapMovieList(movies))
    }
}
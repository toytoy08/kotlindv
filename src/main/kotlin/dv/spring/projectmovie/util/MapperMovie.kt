package dv.spring.projectmovie.util

import dv.spring.projectmovie.entity.*
import dv.spring.projectmovie.entity.dto.*
import org.mapstruct.InheritInverseConfiguration
import org.mapstruct.Mapper
import org.mapstruct.factory.Mappers

@Mapper(componentModel = "movie")
interface MapperMovie {
    companion object {
        val MOVIEINSTANCE = Mappers.getMapper(MapperMovie::class.java)
    }

    fun mapMovie(movie: Movie?): MovieDto
    fun mapMovieList(movies: List<Movie>): List<MovieDto>
//    @InheritInverseConfiguration
//    fun mapMovieDto(movieDto: MovieDto): Movie

    fun mapUser(user: User): UserDto
    fun mapUserList(user: List<User>): List<UserDto>
    @InheritInverseConfiguration
    fun mapUserDto(userDto: UserDto): User

    fun mapShoptime(showtime: Showtime?): ShowtimeDto?
    fun mapShowtimeList(showtime: List<Showtime>): List<ShowtimeDto>
//    @InheritInverseConfiguration
//    fun mapShowtimeDto(showtimeDto: ShowtimeDto):Showtime

    fun mapSeat(seats: Seats?): SeatsDto?
    fun mapSeatList(seats: List<Seats>): List<SeatsDto>
//    @InheritInverseConfiguration
//    fun mapSeatDto(seatsDto: SeatsDto):Seats

    fun mapBooking(booking: Booking): BookingDto
    fun mapBookingList(booking: List<Booking>): List<BookingDto>
//    @InheritInverseConfiguration
//    fun mapBookingDto(bookingDto: BookingDto): Booking

    fun mapHistory(history: History): HistoryDto
    fun mapHistoryList(history: List<History>): List<HistoryDto>
}
package dv.spring.projectmovie.entity

enum class Soundtrack {
    TH, EN , EN_TH
}

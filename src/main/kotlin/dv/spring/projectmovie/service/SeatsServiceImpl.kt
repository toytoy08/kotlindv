package dv.spring.projectmovie.service

import dv.spring.projectmovie.dao.SeatsDao
import dv.spring.projectmovie.entity.Seats
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class SeatsServiceImpl : SeatsService{
    override fun getSeats(): List<Seats> {
        return seatsDao.getSeats()
    }

    @Autowired
    lateinit var seatsDao: SeatsDao
}
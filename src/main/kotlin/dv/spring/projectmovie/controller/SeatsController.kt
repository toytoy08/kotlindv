package dv.spring.projectmovie.controller

import dv.spring.projectmovie.service.SeatsService
import dv.spring.projectmovie.util.MapperMovie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController

class SeatsController{
    @Autowired
    lateinit var seatsService: SeatsService

    @GetMapping("/seat/list")
    fun getSeats(): ResponseEntity<Any> {
        val seats = seatsService.getSeats()
        return ResponseEntity.ok(MapperMovie.MOVIEINSTANCE.mapSeatList(seats))
    }
}
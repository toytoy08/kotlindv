package dv.spring.projectmovie.repository

import dv.spring.projectmovie.entity.History
import org.springframework.data.repository.CrudRepository

interface HistoryRepository :CrudRepository<History,Long>
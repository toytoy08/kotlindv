package dv.spring.projectmovie.repository

import dv.spring.projectmovie.entity.Seats
import org.springframework.data.repository.CrudRepository

interface SeatRepository:CrudRepository<Seats,Long>
package dv.spring.projectmovie.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.Id
import javax.persistence.OneToOne

@Entity
data class History(@OneToOne var booking: Booking? = null) {

    @Id
    @GeneratedValue
    var id: Long? = null

}
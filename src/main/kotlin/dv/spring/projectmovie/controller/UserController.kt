package dv.spring.projectmovie.controller

import dv.spring.projectmovie.entity.dto.UserDto
import dv.spring.projectmovie.service.UserService
import dv.spring.projectmovie.util.MapperMovie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController

class UserController {
    @Autowired
    lateinit var userService: UserService

    @GetMapping("/user/list")
    fun getUsers(): ResponseEntity<Any> {
        val users = userService.getUsers()
        return ResponseEntity.ok(MapperMovie.MOVIEINSTANCE.mapUserList(users))
    }

    @PostMapping("/user/add")
    fun addCustomer(@RequestBody user: UserDto): ResponseEntity<Any> {
        return ResponseEntity.ok(
                MapperMovie.MOVIEINSTANCE.mapUser(
                        userService.save(user)))

    }

    @PutMapping("/user/{userId}")
    fun updateUser(@PathVariable("userId") id: Long?,
                   @RequestBody userDto: UserDto): ResponseEntity<Any> {
        userDto.id = id
        return ResponseEntity.ok(
                MapperMovie.MOVIEINSTANCE.mapUser(userService.save(userDto))
        )
    }
}
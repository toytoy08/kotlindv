package dv.spring.projectmovie.dao

import dv.spring.projectmovie.entity.Showtime
import dv.spring.projectmovie.repository.ShowtimeRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class ShowtimeDaoImpl : ShowtimeDao {
//    override fun getOneShowtimeById(id: Long?): List<Showtime> {
//        return showtimeRepository.findById(id)
//    }

    override fun getShowtimeById(id: Long?): List<Showtime> {
        return showtimeRepository.findByMovie_Id(id)
    }

    override fun getShowtimes(): List<Showtime> {
        return showtimeRepository.findAll().filterIsInstance(Showtime::class.java)
    }

    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository
}
package dv.spring.projectmovie.entity.dto

import dv.spring.projectmovie.entity.Subtitle

data class MovieDto(var id: Long? = null,
                    var name: String? = null,
                    var duration: Int? = null,
                    var image: String? = null,
                    var soundtrack: String? = null,
                    var subtitle: Subtitle?=null
)
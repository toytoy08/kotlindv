package dv.spring.projectmovie.service

import dv.spring.projectmovie.entity.User
import dv.spring.projectmovie.entity.dto.UserDto

interface UserService {
    fun getUsers(): List<User>
    fun save(user: UserDto): User

}
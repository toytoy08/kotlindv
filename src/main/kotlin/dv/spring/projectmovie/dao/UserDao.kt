package dv.spring.projectmovie.dao

import dv.spring.projectmovie.entity.User

interface UserDao {
    fun getUsers(): List<User>
    fun save(userSer: User): User

}
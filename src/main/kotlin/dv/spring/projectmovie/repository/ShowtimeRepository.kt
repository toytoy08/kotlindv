package dv.spring.projectmovie.repository

import dv.spring.projectmovie.entity.Showtime
import org.springframework.data.repository.CrudRepository

interface ShowtimeRepository : CrudRepository<Showtime, Long> {
    fun findByMovie_Id(id: Long?): List<Showtime>
//    fun findById(id: Long?): List<Showtime>
    fun findById(id: Long?): Showtime?
}
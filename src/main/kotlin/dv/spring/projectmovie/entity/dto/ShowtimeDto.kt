package dv.spring.projectmovie.entity.dto

import dv.spring.projectmovie.entity.Movie
import dv.spring.projectmovie.entity.Soundtrack
import dv.spring.projectmovie.entity.Subtitle

data class ShowtimeDto(var movie: Movie? = null,
                       var startDateTime: Long? = null,
                       var endDateTime: Long? = null,
                       var soundtrack: Soundtrack? = null,
                       var subtitle: Subtitle? = null,
//                       var seats: List<SeatsDto>? = null,
                       var id: Long? = null)
package dv.spring.projectmovie.dao

import dv.spring.projectmovie.entity.Movie
import dv.spring.projectmovie.repository.MovieRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class MovieDaoImpl: MovieDao {
    override fun getMovies(): List<Movie> {
        return movieRepository.findAll().filterIsInstance(Movie::class.java)
    }

    @Autowired
    lateinit var movieRepository: MovieRepository
}
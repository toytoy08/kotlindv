package dv.spring.projectmovie.entity.dto

data class UserDto(var email: String? = null,
                   var firstName: String? = null,
                   var lastName: String? = null,
                   var image: String? = "https://i.imgur.com/xoFYkyq.png",
                   var id: Long? = null)
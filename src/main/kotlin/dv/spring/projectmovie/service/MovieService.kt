package dv.spring.projectmovie.service

import dv.spring.projectmovie.entity.Movie

interface MovieService{
    fun getMovies(): List<Movie>
}
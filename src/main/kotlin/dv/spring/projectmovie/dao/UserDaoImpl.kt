package dv.spring.projectmovie.dao

import dv.spring.projectmovie.entity.User
import dv.spring.projectmovie.entity.dto.UserDto
import dv.spring.projectmovie.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository

@Profile("db")
@Repository
class UserDaoImpl : UserDao {
    override fun save(userSer: User): User {
        return userRepository.save(userSer)
    }

    override fun getUsers(): List<User> {
        return userRepository.findAll().filterIsInstance(User::class.java)
    }

    @Autowired
    lateinit var userRepository: UserRepository
}
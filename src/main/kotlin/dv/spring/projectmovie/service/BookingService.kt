package dv.spring.projectmovie.service

import dv.spring.projectmovie.entity.Booking

interface BookingService{
    fun getBooking(): List<Booking>
    fun save(id: Long?): Booking

}
package dv.spring.projectmovie.controller

import dv.spring.projectmovie.service.BookingService
import dv.spring.projectmovie.util.MapperMovie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController

class BookingController {
    @Autowired
    lateinit var bookingService: BookingService

    @GetMapping("/booking/list")
    fun getBooking(): ResponseEntity<Any> {
        val booking = bookingService.getBooking()
        return ResponseEntity.ok(MapperMovie.MOVIEINSTANCE.mapBookingList(booking))
    }

    @PostMapping("/booking/add/{showId}")
    fun addBooking(@PathVariable("showId") id: Long?): ResponseEntity<Any> {
        val booking = bookingService.save(id)
        return ResponseEntity.ok(MapperMovie.MOVIEINSTANCE.mapBooking(booking))
    }
}
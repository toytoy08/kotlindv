package dv.spring.projectmovie.service

import dv.spring.projectmovie.dao.BookingDao
import dv.spring.projectmovie.dao.HistoryDao
import dv.spring.projectmovie.entity.History
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class HistoryServiceImpl : HistoryService{
    override fun getHistory(): List<History> {
        return historyDao.getHistory()
    }

    @Autowired
    lateinit var historyDao: HistoryDao
}
package dv.spring.projectmovie.entity

import javax.persistence.*

@Entity
data class Booking(
        var createdDateTime: Long? = null
) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToMany
    var user = mutableListOf<User>()

    @ManyToOne
    var showtime: Showtime? = null

    @ManyToMany
    var seats = mutableListOf<Seats>()

}
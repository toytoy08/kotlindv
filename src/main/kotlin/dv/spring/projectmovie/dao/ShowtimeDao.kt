package dv.spring.projectmovie.dao

import dv.spring.projectmovie.entity.Showtime

interface ShowtimeDao{
    fun getShowtimes(): List<Showtime>
    fun getShowtimeById(id: Long?): List<Showtime>
//    fun getOneShowtimeById(id: Long?): List<Showtime>

}
package dv.spring.projectmovie.service

import dv.spring.projectmovie.entity.Showtime

interface ShowtimeService{
    fun getShowtimes(): List<Showtime>
    fun getShowtimeById(id: Long?): List<Showtime>
//    fun getOneShowtimeById(id: Long?): List<Showtime>

}
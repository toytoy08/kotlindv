package dv.spring.projectmovie.dao

import dv.spring.projectmovie.entity.Seats

interface SeatsDao{
    fun getSeats(): List<Seats>

}
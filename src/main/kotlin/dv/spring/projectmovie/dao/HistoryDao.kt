package dv.spring.projectmovie.dao

import dv.spring.projectmovie.entity.History

interface HistoryDao{
    fun getHistory(): List<History>

}
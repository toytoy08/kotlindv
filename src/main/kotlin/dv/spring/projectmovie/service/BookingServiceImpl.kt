package dv.spring.projectmovie.service

import dv.spring.projectmovie.dao.BookingDao
import dv.spring.projectmovie.dao.ShowtimeDao
import dv.spring.projectmovie.entity.Booking
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class BookingServiceImpl : BookingService {
    override fun save(id: Long?): Booking {
        return bookingDao.save(id)
    }

    override fun getBooking(): List<Booking> {
        return bookingDao.getBooking()
    }

    @Autowired
    lateinit var bookingDao: BookingDao
    @Autowired
    lateinit var showtimeDao: ShowtimeDao
}
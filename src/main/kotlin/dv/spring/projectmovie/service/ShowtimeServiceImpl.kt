package dv.spring.projectmovie.service

import dv.spring.projectmovie.dao.ShowtimeDao
import dv.spring.projectmovie.entity.Showtime
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service

@Service
class ShowtimeServiceImpl:ShowtimeService{
//    override fun getOneShowtimeById(id: Long?): List<Showtime> {
//        return showtimeDao.getOneShowtimeById(id)
//    }

    override fun getShowtimeById(id: Long?): List<Showtime> {
        return showtimeDao.getShowtimeById(id)
    }

    override fun getShowtimes(): List<Showtime> {
        return showtimeDao.getShowtimes()
    }

    @Autowired
    lateinit var showtimeDao: ShowtimeDao
}
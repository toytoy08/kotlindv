package dv.spring.projectmovie.dao

import dv.spring.projectmovie.entity.Movie

interface MovieDao{
    fun getMovies(): List<Movie>

}
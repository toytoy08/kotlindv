package dv.spring.projectmovie.dao

import dv.spring.projectmovie.entity.Booking
import dv.spring.projectmovie.entity.History
import dv.spring.projectmovie.repository.BookingRepository
import dv.spring.projectmovie.repository.HistoryRepository
import dv.spring.projectmovie.repository.ShowtimeRepository
import dv.spring.projectmovie.repository.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Profile
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Profile("db")
@Repository
class BookingDaoImpl : BookingDao {
    @Transactional
    override fun save(id: Long?): Booking {
        val showtime = showtimeRepository.findById(id)
        val time = System.currentTimeMillis()
        val booking = bookingRepository.save(Booking(time))
        booking.showtime = showtime
        val history = historyRepository.save(History(booking))
        return booking
    }

    override fun getBooking(): List<Booking> {
        return bookingRepository.findAll().filterIsInstance(Booking::class.java)
    }

    @Autowired
    lateinit var bookingRepository: BookingRepository
    @Autowired
    lateinit var showtimeRepository: ShowtimeRepository
    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var historyRepository: HistoryRepository
}
package dv.spring.projectmovie.entity.dto

import dv.spring.projectmovie.entity.Booking

data class HistoryDto(var id: Long? = null,
                      var booking: Booking? = null
)
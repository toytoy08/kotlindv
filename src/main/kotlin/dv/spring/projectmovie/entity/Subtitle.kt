package dv.spring.projectmovie.entity

enum class Subtitle {
    EN, TH
}
package dv.spring.projectmovie.dao

import dv.spring.projectmovie.entity.Booking

interface BookingDao{
    fun getBooking(): List<Booking>
    fun save(id: Long?): Booking
}
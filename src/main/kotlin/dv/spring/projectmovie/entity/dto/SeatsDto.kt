package dv.spring.projectmovie.entity.dto

import dv.spring.projectmovie.entity.SeatType

data class SeatsDto(var id: Long? = null,
                    var name: String? = null,
                    var type: SeatType? = null,
                    var price: Int? = null,
                    var rowseat: Int? = null,
                    var columnseat: Int? = null
)
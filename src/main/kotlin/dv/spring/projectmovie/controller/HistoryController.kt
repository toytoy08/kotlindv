package dv.spring.projectmovie.controller

import dv.spring.projectmovie.service.HistoryService
import dv.spring.projectmovie.util.MapperMovie
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController

class HistoryController {
    @Autowired
    lateinit var historyService: HistoryService

    @GetMapping("/history/list")
    fun getHistory(): ResponseEntity<Any> {
        val history = historyService.getHistory()
        return ResponseEntity.ok(MapperMovie.MOVIEINSTANCE.mapHistoryList(history))
    }
}
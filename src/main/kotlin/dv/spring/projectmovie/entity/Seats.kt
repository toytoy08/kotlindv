package dv.spring.projectmovie.entity

import javax.persistence.*

@Entity
data class Seats(var name: String? = null,
                 var type: SeatType? = null,
                 var price: Int? = null,
                 var rowseat: Int? = null,
                 var columnseat: Int? = null) {
    @Id
    @GeneratedValue
    var id: Long? = null

    @OneToOne
    var user: User? = null
}